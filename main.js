include("util.js");

var karotz_ip = 'localhost';
var debugvm = false;

if(typeof(instanceName)=='undefined'){
  include("vm.js");
}


var buttonListener = function(event) {
    if (event == "DOUBLE") {
        karotz.tts.stop();
        exit();
    }
    return true;
};

var exitFunction = function(event) {
    if((event == "CANCELLED") || (event == "TERMINATED")) {
        exit();
    }
    return true;
};

var onKarotzConnect = function(data) {
    var api, lat, long, url;
    karotz.button.addListener(buttonListener);

    api = params[instanceName].api;
    lat = params[instanceName].lat;
    long = params[instanceName].long;

    url = "https://api.darkskyapp.com/v1/forecast/" + api + "/" + lat + "," + long;
    data = JSON.parse(http.get(url));
    text = "It is currently " + data['currentSummary'] + ".  Over the next hour, " + data['hourSummary'] + ".";
    text += ". .This forecast provided by the Dark Sky API.";
    karotz.tts.start(text, "en", exitFunction);
};

karotz.connectAndStart(karotz_ip, 9123, onKarotzConnect, {});
